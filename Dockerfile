FROM goreleaser/goreleaser:v1.11.2

## Unsure how to do this in a way that passes the linter...🤔
# hadolint ignore=DL3022
COPY --from=anchore/syft:v0.46.1@sha256:2edcb655d3eada83dce419f55811254bfd83161940bcd7cc3bd98f0609d0c8eb /syft /usr/local/bin/syft
# hadolint ignore=DL3022
COPY --from=library/vault:1.10.3@sha256:74a6f9588b4d1c548ea4bd8731fdd04d0356f3d607f2e1bc9408f6c75e78d85c /bin/vault /usr/local/bin/vault
# hadolint ignore=DL3022
COPY --from=mikefarah/yq:4.27.2@sha256:23fdacd7da0a4f7b2bae9583e5ab610227a978c38f4b1462f2f1053af6a9573c /usr/bin/yq /usr/local/bin/yq

RUN go install golang.org/x/vuln/cmd/govulncheck@latest

